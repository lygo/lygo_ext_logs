package lygo_ext_logs

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t a n t s
//----------------------------------------------------------------------------------------------------------------------

// Program Workspace (child) logging folder. Here are all logs
const ROOT string = "logging"

const (
	FORMATTER_TEXT = 0
	FORMATTER_JSON = 1
)

const (
	LEVEL_PANIC = iota
	LEVEL_ERROR
	LEVEL_WARN
	LEVEL_INFO
	LEVEL_DEBUG
	LEVEL_TRACE
)

const (
	OUTPUT_CONSOLE = iota
	OUTPUT_FILE
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type LogParams struct {
	Level int
	Args  []interface{}
}

type LogContext struct {
	Caller string
	Data   interface{}
}

//----------------------------------------------------------------------------------------------------------------------
//	f i e l d s
//----------------------------------------------------------------------------------------------------------------------

var _logger *Logger // main logger
var _root, _fileName string

//----------------------------------------------------------------------------------------------------------------------
//	i n i t
//----------------------------------------------------------------------------------------------------------------------

func init() {
	_logger = NewLogger()
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func GetRoot() string {
	return _root
}

func GetFileName() string {
	return _fileName
}

func SetFormatter(value int) {
	if nil != _logger {
		_logger.SetFormatter(value)
	}
}

func SetLevel(value int) {
	if nil != _logger {
		_logger.SetLevel(value)
	}
}

func SetLevelName(value string) {
	if nil != _logger {
		_logger.SetLevelName(value)
	}
}

func GetLevel() int {
	if nil != _logger {
		return _logger.GetLevel()
	}
	return LEVEL_INFO
}

func SetOutput(value int) {
	if nil != _logger {
		_logger.SetOutput(value)
	}
}

func Close() {
	if nil != _logger {
		_logger.Close()
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	l o g g i n g
//----------------------------------------------------------------------------------------------------------------------

func Panic(args ...interface{}) {
	if nil!=_logger{
		_logger.Panic(args...)
	}
}

func Error(args ...interface{}) {
	if nil!=_logger{
		_logger.Error(args...)
	}
}

func Warn(args ...interface{}) {
	if nil!=_logger{
		_logger.Warn(args...)
	}
}

func Info(args ...interface{}) {
	if nil!=_logger{
		_logger.Info(args...)
	}
}

func Debug(args ...interface{}) {
	if nil!=_logger{
		_logger.Debug(args...)
	}
}

func Trace(args ...interface{}) {
	if nil!=_logger{
		_logger.Trace(args...)
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
