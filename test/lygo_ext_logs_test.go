package test

import (
	"bitbucket.org/lygo/lygo_ext_logs"
	"testing"
	"time"
)

func TestSimple(t *testing.T) {

	lygo_ext_logs.SetLevel(lygo_ext_logs.LEVEL_INFO)

	lygo_ext_logs.Info("Test Info Log Message")
	lygo_ext_logs.Warn("Test Warn Log Message")

	lygo_ext_logs.Close()

	time.Sleep(3*time.Second)

}

func TestLogger(t *testing.T) {

	logger1 := lygo_ext_logs.NewLogger()
	logger1.SetLevel(lygo_ext_logs.LEVEL_INFO)
	logger1.Info("logger1 Test Info Log Message")
	logger1.Warn("logger1 Test Warn Log Message")
	logger1.Close()

	logger2 := lygo_ext_logs.NewLogger()
	logger2.SetLevel(lygo_ext_logs.LEVEL_WARN)
	logger2.Info("logger2 Test Info Log Message")
	logger2.Warn("logger2 Test Warn Log Message")
	logger2.Close()

	time.Sleep(3*time.Second)

}

func TestFile(t *testing.T) {
	logger1 := lygo_ext_logs.NewLogger()
	logger1.SetLevel(lygo_ext_logs.LEVEL_INFO)
	logger1.SetFileName("./logging.log")

	logger1.Info("logger1 Test Info Log Message")
	logger1.Warn("logger1 Test Warn Log Message")
	logger1.Close()

	time.Sleep(3*time.Second)
}
