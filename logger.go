package lygo_ext_logs

import (
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type Logger struct {
	logger      *logrus.Logger // main logger
	initialized bool
	fileName    string
	formatter   int
	level       int
	output      int
	channel     chan *LogParams
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewLogger() *Logger {
	instance := new(Logger)
	instance.initialized = false

	instance.logger = logrus.New()
	instance.logger.SetOutput(os.Stdout)
	instance.logger.SetLevel(logrus.InfoLevel)
	instance.logger.SetReportCaller(false)

	instance.SetFormatter(FORMATTER_TEXT)
	instance.SetLevel(LEVEL_INFO)
	instance.SetOutput(OUTPUT_CONSOLE)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Logger) GetFileName() string {
	return instance.fileName
}

func (instance *Logger) SetFileName(path string) {
	instance.fileName = path
	if len(path) > 0 {
		instance.output = OUTPUT_FILE
	}
}

func (instance *Logger) SetFormatter(value int) {
	instance.formatter = value
	switch instance.formatter {
	case FORMATTER_TEXT:
		instance.logger.SetFormatter(&logrus.TextFormatter{})
	case FORMATTER_JSON:
		instance.logger.SetFormatter(&logrus.JSONFormatter{})
	default:
		instance.logger.SetFormatter(&logrus.TextFormatter{})
	}
}

func (instance *Logger) SetLevel(value int) {
	instance.level = value
	switch instance.level {
	case LEVEL_PANIC:
		instance.logger.SetLevel(logrus.PanicLevel)
	case LEVEL_ERROR:
		instance.logger.SetLevel(logrus.ErrorLevel)
	case LEVEL_WARN:
		instance.logger.SetLevel(logrus.WarnLevel)
	case LEVEL_INFO:
		instance.logger.SetLevel(logrus.InfoLevel)
	case LEVEL_DEBUG:
		instance.logger.SetLevel(logrus.DebugLevel)
	case LEVEL_TRACE:
		instance.logger.SetLevel(logrus.TraceLevel)
	default:
		instance.logger.SetLevel(logrus.WarnLevel)
	}
}

func (instance *Logger) SetLevelName(value string) {
	switch value {
	case "panic":
		instance.logger.SetLevel(logrus.PanicLevel)
		instance.level = LEVEL_PANIC
	case "error":
		instance.logger.SetLevel(logrus.ErrorLevel)
		instance.level = LEVEL_ERROR
	case "warn":
		instance.logger.SetLevel(logrus.WarnLevel)
		instance.level = LEVEL_WARN
	case "info":
		instance.logger.SetLevel(logrus.InfoLevel)
		instance.level = LEVEL_INFO
	case "debug":
		instance.logger.SetLevel(logrus.DebugLevel)
		instance.level = LEVEL_DEBUG
	case "trace":
		instance.logger.SetLevel(logrus.TraceLevel)
		instance.level = LEVEL_TRACE
	default:
		instance.logger.SetLevel(logrus.WarnLevel)
		instance.level = LEVEL_WARN
	}
}

func (instance *Logger) GetLevel() int {
	return instance.level
}

func (instance *Logger) SetOutput(value int) {
	instance.output = value
}

func (instance *Logger) Close() {
	if nil != instance.channel {
		close(instance.channel)
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	l o g g i n g
//----------------------------------------------------------------------------------------------------------------------

func (instance *Logger) Panic(args ...interface{}) {
	instance.initialize()

	logParams := new(LogParams)
	logParams.Level = LEVEL_PANIC
	logParams.Args = args

	if nil != instance.channel {
		instance.channel <- logParams
	}
}

func (instance *Logger) Error(args ...interface{}) {
	instance.initialize()

	logParams := new(LogParams)
	logParams.Level = LEVEL_ERROR
	logParams.Args = args
	if nil != instance.channel {
		instance.channel <- logParams
	}
}

func (instance *Logger) Warn(args ...interface{}) {
	instance.initialize()

	logParams := new(LogParams)
	logParams.Level = LEVEL_WARN
	logParams.Args = args
	if nil != instance.channel {
		instance.channel <- logParams
	}
}

func (instance *Logger) Info(args ...interface{}) {
	instance.initialize()

	logParams := new(LogParams)
	logParams.Level = LEVEL_INFO
	logParams.Args = args
	if nil != instance.channel {
		instance.channel <- logParams
	}
}

func (instance *Logger) Debug(args ...interface{}) {
	instance.initialize()

	logParams := new(LogParams)
	logParams.Level = LEVEL_DEBUG
	logParams.Args = args
	if nil != instance.channel {
		instance.channel <- logParams
	}
}

func (instance *Logger) Trace(args ...interface{}) {
	instance.initialize()

	logParams := new(LogParams)
	logParams.Level = LEVEL_TRACE
	logParams.Args = args
	if nil != instance.channel {
		instance.channel <- logParams
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Logger) initialize() {
	if instance.initialized {
		return
	}
	instance.initialized = true

	// set logging file
	if len(instance.fileName) == 0 {
		var workspace string = lygo_paths.GetWorkspacePath()
		instance.fileName = lygo_paths.Concat(lygo_paths.Concat(workspace, ROOT), "logging.log")
	}

	// init the buffered channel
	instance.channel = make(chan *LogParams, 10)
	go instance.receive(instance.channel)
}

func (instance *Logger) receive(ch <-chan *LogParams) {
	// loop until channel is open
	for arg := range ch {
		instance.doLog(arg.Level, arg.Args...)
	}
}

func (instance *Logger) doLog(level int, args ...interface{}) {
	// ensure initialization
	instance.initialize()

	// init write on file
	if len(instance.fileName) > 0 && instance.output == OUTPUT_FILE {
		_ = lygo_paths.Mkdir(instance.fileName)

		file, err := os.OpenFile(instance.fileName, os.O_APPEND|os.O_CREATE|os.O_RDWR, os.ModePerm) // 0644
		if err != nil {
			instance.logger.SetOutput(os.Stderr)
			instance.logger.Fatal(err)
		} else {
			// defer to close the file
			defer file.Close()
			instance.logger.SetOutput(file)
		}
	} else {
		instance.logger.SetOutput(os.Stdout)
	}

	// log context
	fields, message := instance.parseContext(args...)
	// fields := logrus.Fields{}

	// do log
	switch level {
	case LEVEL_PANIC:
		instance.logger.WithFields(fields).Panic(message)
	case LEVEL_ERROR:
		instance.logger.WithFields(fields).Error(message)
	case LEVEL_WARN:
		instance.logger.WithFields(fields).Warn(message)
	case LEVEL_INFO:
		instance.logger.WithFields(fields).Info(message)
		// _logger.Info(message)
	case LEVEL_DEBUG:
		instance.logger.WithFields(fields).Debug(message)
	case LEVEL_TRACE:
		instance.logger.WithFields(fields).Trace(message)
	default:
		instance.logger.WithFields(fields).Info(message)
	}

}

func (instance *Logger) parseContext(args ...interface{}) (fields logrus.Fields, message string) {
	switch len(args) {
	case 1:
		message = fmt.Sprintf("%v", args[0])
		fields = logrus.Fields{}
	case 2:
		message = fmt.Sprintf("%v", args[1])
		context, ok := args[0].(LogContext)
		if ok {
			fields = logrus.Fields{
				"caller": context.Caller,
				"data":   context.Data,
			}
		} else {
			fields = logrus.Fields{}
		}
	default:
		message = fmt.Sprintf("%v", args[0])
		fields = logrus.Fields{}
	}

	return fields, message
}
