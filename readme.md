# Logs
Simple logging module based on Logrus framework

## How to Use

To use just call:

```
go get -u bitbucket.org/lygo/lygo_ext_logs
```

## Dependencies

Depends on 
Library: [Logrus](https://github.com/sirupsen/logrus)

`go get -u github.com/sirupsen/logrus`

`go get -u bitbucket.org/lygo/lygo_commons`

### Versioning

Sources are versioned using git tags:

```
git tag v0.1.9
git push origin v0.1.9
```