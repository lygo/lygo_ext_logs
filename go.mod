module bitbucket.org/lygo/lygo_ext_logs

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	github.com/google/uuid v1.3.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/sys v0.0.0-20210909193231-528a39cd75f3 // indirect
)
